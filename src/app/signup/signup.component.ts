import { Component } from '@angular/core'
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../user.service';


@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html'
})
export class SignupComponent {
    private signupForm: FormGroup = new FormGroup ({
        first_name: new FormControl(),
        last_name: new FormControl(),
        email_init: new FormControl('', [Validators.required]),
        username: new FormControl('', [Validators.required]),
        password: new FormControl('', [Validators.required])
    });

    constructor(private userService: UserService){
    }

    doSignUp(){
        if (this.signupForm.valid){
            console.log(this.signupForm.value)
            this.userService.signup(this.signupForm.value).subscribe(user => {
                console.log(user)
            }, err => {
                console.log(err.error)
            });
        }
    }
}