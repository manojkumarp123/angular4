import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service'

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {

  private users: any;
  constructor(private user: UserService) { }

  ngOnInit() {
    this.user.getUsers().subscribe(users => {
      this.users = users;
    })
  }

}
