import { Injectable, EventEmitter } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpClient} from '@angular/common/http';
import { User } from './user'
import { Observable } from "rxjs/Observable";
import 'rxjs/add/operator/map';

@Injectable()
export class UserService {

    // private is_logged_in = false;
    // public token = new EventEmitter;
    public token: string;

    constructor(private http: HttpClient) { }

    signup(userData: any){
        return this.http.post('/api/users/', userData)
    }

    login(userData): Observable<boolean> {
        return this.http.post('/api-token-auth', userData)
            .map((response: Response) => {
                console.log(response)
                // login successful if there's a jwt token in the response
                let token: string = response && response["token"];
                if (token) {
                    // set token property
                    this.token = token;
 
                    // store username and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUserToken', token);
 
                    // return true to indicate successful login
                    return true;
                } else {
                    // return false to indicate failed login
                    return false;
                }
            });
    }

    // login(userData: any){
    //     this.http.post('/api-token-auth', userData).subscribe(data => {
    //         // this.token.emit(data["token"])
    //         console.log(data["token"])
    //     })
    // }

    getUsers(){
        return this.http.get('/api/users/')
    }

}

export class Misc{

    static removeEmpty(obj) {
        Object.keys(obj).forEach((k) => (!obj[k] && obj[k] !== undefined) && delete obj[k]);
        return obj;
    }
}


@Injectable()
export class DomainInterceptor implements HttpInterceptor {

    private DOMAIN = "http://localhost:8000"

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        let obj: any = {
            url: this.DOMAIN + req.url
        }

        if("body" in obj){
            obj["body"] = Misc.removeEmpty(req.body)
        }

        let currentUserToken: string = localStorage.getItem('currentUserToken')
        // console.log(currentUserToken)
        // if (currentUserToken){
        //     obj["header"] = req.headers.set('Authentication', 'JWT ' + currentUserToken)
        //     console.log(obj)
        // }

        if (currentUserToken){
            obj["header"] = req.headers.set('Authentication', 'JWT ' + currentUserToken)
        }

        const changedReq = req.clone(obj)
        return next.handle(changedReq);
    }
}