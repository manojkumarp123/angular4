import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UserService } from '../user.service'

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html'
})
export class LoginComponent {
    private loginForm: FormGroup = new FormGroup ({
        username: new FormControl('manoj', [Validators.required]),
        password: new FormControl('123', [Validators.required])
    });

    constructor(private userService: UserService){

    }

    doLogin(){

        if (this.loginForm.valid){
            this.userService.login(this.loginForm.value).subscribe(res => {
                console.log(res)
            })
        }
    }
    

}